Django==1.11.5
django-cors-headers==2.1.0
djangorestframework==3.6.4
ez-setup==0.9
mysqlclient==1.3.12
PyMySQL==0.7.11
pytz==2017.2
