import { FormControl, AbstractControl } from '@angular/forms';
import * as mo from 'moment';

export interface IDictionary<T> {
    add(key: string, value: T): void;
    get(key:string) : T;
    remove(key: string): void;
    containsKey(key: string): boolean;
    keys(): string[];
    values(): T[];
}

export class Dictionary<T> implements IDictionary<T> {
    _dict: { [Key: string] : T };

    constructor(init?: { Key: string, Value:T }[]) {
        this._dict = { };
        if (init) {
            for (var i = 0; i < init.length; i++) {
                this._dict[init[i].Key] = init[i].Value;
            }
        }
    }

    add(key: string, value: T): void {
        if (!this._dict[key]) {
            this._dict[key] = value;
        }
    }

    get(key:string): T {
        if (this._dict[key]) {
            return this._dict[key];
        }
        throw "The key does not exist in the dictionary";
    }


    remove(key: string): void { 
        if (this._dict[key]) {
            delete this._dict[key];
        } else {
            throw "The key to delete does not exist";
        }
    }

    containsKey(key: string): boolean {
        return this._dict[key] !== null;
    } 

    keys(): string[] {
        var keys: string[] = [];

        for (var key in this._dict) {
            if (key) {
                keys.push(key);
            }
        }

        return keys;
    }

    values(): T[] {
        var keys = this.keys();
        var values: T[] = [];
        for (var i = 0; i < keys.length; i++) {
            values.push(this._dict[keys[i]]);
        }
        return values;
    }
}

// Return a function that checks if a given date string is valid given a particular format (i.e. DD/MM/YYYY)
export function checkIfValidDateFunction(format:string) {
    return function (date: string) {
        let parsedDate = mo(date, format);
        return parsedDate.isValid;
    }
}

export function conditionalIfValidator(isValid: Function) {
    function validate(control: AbstractControl): { [key: string]: any } {
        return isValid(control.value) ? null : { 'conditionallyInvalid': { value: control.value } };
    }

    return validate;
}

