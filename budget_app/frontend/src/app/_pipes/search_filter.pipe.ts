import { Injectable, Pipe, PipeTransform } from '@angular/core';


// https://hassantariqblog.wordpress.com/2017/03/16/angular2-creating-custom-search-filter-pipe-for-ngfor-directive/

@Pipe({
    name: 'textSearchFilter'
})
@Injectable()
export class TextSearchFilterPipe implements PipeTransform {
    transform(items: any[], field: string, comparer: Function): any[] {
        if (!items) return items;
        return items.filter(item => comparer(item[field]));
    }
}