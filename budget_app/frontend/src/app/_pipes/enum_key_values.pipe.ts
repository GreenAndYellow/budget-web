import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "enumKeyValues"
})
export class EnumKeyValuesPipe implements PipeTransform {
    transform(enumValues: any[], args: string[]): any {
        let keyValues = [];

        for (var enumValue in enumValues) {
            if (typeof(enumValue) == 'string' || !isNaN(parseInt(enumValue, 10))) {
                keyValues.push({ key: enumValue, value: enumValues[enumValue] });
            }
        }

        return keyValues;
    }

}