import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/index';

import { HomeComponent } from './components/home/index';

import { BudgetHomeComponent, UserBudgetSettingsComponent } from './components/budget/index';

import { PageLayoutComponent } from './components/common/index';

import { AuthGuard } from './_guards/index';

const appRoutes: Routes = [
	{ path: 'login', component: LoginComponent },
	// { path: 'home', component: HomeComponent },
	{ path: '', component: PageLayoutComponent, canActivate: [AuthGuard], children: [
			{ path: 'home', component: HomeComponent },
			{ path: 'budget_home', component:  BudgetHomeComponent },
			{ path: 'user_settings', component: UserBudgetSettingsComponent }
		]
	},
	{ path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(appRoutes);