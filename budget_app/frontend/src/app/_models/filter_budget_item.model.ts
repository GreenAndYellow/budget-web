import { BudgetItemModel } from './budget_item.model';
import * as mo from 'moment';

export class FilterBudgetItemModel {
    // Budgets are set as moment objects
    private budgetDateFrom: mo.Moment;
    private budgetDateTo: mo.Moment;
    public searchField: string = "";

    // Create a filter budget item service

    passesFilter(budgetItem : BudgetItemModel) : boolean {
        var searchEquals = true;

        Object.keys(budgetItem).forEach(key => {
            if (key == "_date") {
                let budgetDateFromCheck = this.budgetDateFrom.isSameOrBefore(budgetItem.date) || !this.budgetDateFrom;
                let budgetDateToCheck = this.budgetDateTo.isSameOrAfter(budgetItem.date) || !this.budgetDateTo;

                // Check moment date range
                if (!budgetDateFromCheck || !budgetDateToCheck) {
                    searchEquals = false;
                    return false;
                }
            } else if (this[key] && this[key] !== budgetItem[key]) {
                searchEquals = false;
                return false;
            }
        });

        return searchEquals;
    }

    passesQuickSearch(budgetItem: BudgetItemModel): boolean {
        return (this.searchField == "" || budgetItem.name.includes(this.searchField) || budgetItem.description.includes(this.searchField));
    }


    get formattedBudgetDateFrom() : any {
        if (this.budgetDateFrom) {
            return this.budgetDateFrom.format("DD/MM/YYYY");
        } else {
            return null;
        }
    }

    get formattedBudgetDateTo() : any {
        if (this.budgetDateTo) {
            return this.budgetDateTo.format("DD/MM/YYYY");
        } else {
            return null;
        }
    }

    // set formattedBudgetDateFrom(date: any) {
    //     if (date && date !== "") {
    //         this.budgetDateFrom = mo(date, "DD/MM/YYYY");
    //     }
    // }

    // set formattedBudgetDateTo(date: any) {
    //     if (date && date !== "") {
    //         this.budgetDateTo = mo(date, "DD/MM/YYYY");
    //     }
    // }

    public setFormattedBudgetDateFrom(date: any, format?: string) {
        if (date instanceof Date) {
            this.budgetDateFrom = mo(date);
        } else {
            let formattedFromDate = mo(date, format);
            if (formattedFromDate.isValid()) {
                this.budgetDateFrom = formattedFromDate;
            }
        }
    }

    public setFormattedBudgetDateTo(date: any, format?: string) {
        if (date instanceof Date) {
            this.budgetDateTo = mo(date);
        } else {
            let formattedToDate = mo(date, format);
            if (formattedToDate.isValid()) {
                this.budgetDateTo = formattedToDate;
            }
        }
    }
}