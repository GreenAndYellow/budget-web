import { DaysOfWeek } from "../_enums/index";

import * as mo from 'moment';

export class UserBudgetSettingModel {
    public id: number = -1;
    public cycleLength: number = null;
    public cycleBudget: number = null;
    public cycleStartDay: DaysOfWeek = DaysOfWeek.Monday;
    private _cycleStartDate: Date = null;

    constructor(init?: any) {
        this.cycleLength = 7;
        if (init) {
            // Convert date if given as string from database
            if (init.cycleStartDate && typeof(init.cycleStartDate) == 'string') {
                init.cycleStartDate = mo(init.cycleStartDate, 'YYYY-MM-DD').toDate();
            }
            Object.assign(this, init);
        }
    }

    get JSONString() {
        let serializeObject = {
            id: this.id,
            cycleLength: this.cycleLength,
            cycleBudget: this.cycleBudget,
            cycleStartDay: this.cycleStartDay,
            cycleStartDate: this.getFormattedCycleStartDate('YYYY-MM-DD')
        }

        return JSON.stringify(serializeObject);
    }
    
    get formattedCycleStartDate() {
        return mo(this.cycleStartDate).format("DD/MM/YYYY");
    }

    get cycleStartDate() {
        return this._cycleStartDate;
    }

    set cycleStartDate(date: Date) {
        if (!(date instanceof Date)) {
            throw "cycleStartDate setter expects a date parameter of type Date"
        }
        this._cycleStartDate = date;
    }

    public setFormattedCycleStartDate(date: string, format:string) {
        var formattedDate = mo(date, format);
        if (formattedDate.isValid()) {
            this._cycleStartDate = formattedDate.toDate();
        } else {
            throw "Cannot set cycle start date, provided format is invalid";
        }
    }

    public getFormattedCycleStartDate(format: string) {
        var formattedDate = mo(this.cycleStartDate);
        if (formattedDate.isValid()) {
            return formattedDate.format(format);
        } else {
            throw "Cannot get cycle start date, provided format is invalid";
        }
    }
    
}