
export class BudgetBalanceModel {
    constructor(public credit: number, public debit: number, public budget: number) {
    }
}