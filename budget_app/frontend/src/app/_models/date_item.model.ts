export class DateItemModel {
    // Assume numbers being passed in start from 1
    private _date: Date;

    private _year: number;
    private _month: number;
    private _day: number;
    
    constructor(year?: number, month: number = 0, day: number = 1) {
        this._year = year;
        this._month = month;
        this._day = day;

        if (year != null) {
            this._date = new Date(year, month, day);
        } else {
            this._date = new Date();
        }
    }

    set year(year: number) {
        if (year == null) {
            this._year = year;
            this.month = null;
            this.day = null;
        } else {
            this._date.setFullYear(year);
            this._year = this._date.getFullYear();
        }
    }

    set month(month: number) {
        if (month == null) {
            this._month = month;
            this.day = null;
        } else {
            this._date.setMonth(month);
            this._month = this._date.getMonth();
        }
    }

    set day(day: number) {
        if (day == null) {
            this._day = day;
        } else {
            this._date.setDate(day);
            this._day = this._date.getDate();
        }
    }

    get year() {
        return this._year;
    }

    get month() {
        return this._month;
    }

    get day() {
        return this._day;
    }

    setDate(year: number, month: number, day: number) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    get date() {
        // return this._date;
        return { year: this._year, month: this._month, day: this._day };
    }

}