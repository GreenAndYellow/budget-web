import { DateItemModel } from './'

import * as mo from 'moment';


export class DateRangeModel {
    private _fromDate: Date;
    private _toDate: Date;

    constructor(fromDate?: Date, toDate?: Date) {
        this._fromDate = fromDate ? new Date(fromDate) : new Date();
        this._toDate = toDate ? new Date(toDate) : new Date();
    }

    set fromDate(date: Date) {
        this._fromDate = date;
    }


    set toDate(date: Date) {
        this._toDate = date;
    }


    get fromDate() {
        return this._fromDate;
    }

    get toDate() {
        return this._toDate;
    }

    getFormattedFromDate(format: string) {
        return mo(this._fromDate).format(format);
    }

    getFormattedToDate(format: string) {
        return mo(this._toDate).format(format);
    }
}