import { UserBudgetSettingModel } from "..";
import * as $ from 'jquery';

export class BudgetPeriodSelectorSettingModel {
    parentElement: JQuery = null;
    initYear: number = 1994;
    pageLength: number = 6;
    userBudgetSetting: UserBudgetSettingModel = null;
    callback: Function;

    constructor(init?: Object) {
        if (init) {
            Object.assign(this, init);
        }
    }
}