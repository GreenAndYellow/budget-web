import * as mo from 'moment';
import { CommonBudgetInfo } from './common_budget_info.model'


export class RecurringBudgetItemModel extends CommonBudgetInfo {
    public cycleLength: number;
    private _startDate: mo.Moment;
    private _endDate: mo.Moment;

    constructor(init?: any) {
        console.log(init);
        super(init);
        if (init) {
            if (!init.startDate) {
                throw "Start date must be provided for RecurringBudgetItemModel";
            }
            this._startDate = mo(init.startDate);

            this._endDate = init.endDate ? mo(init.endDate) : null; 
            this.cycleLength = init.cycleLength;
        }
    }

    set startDate(startDate: Date) {
        this._startDate = mo(startDate);
    }

    get startDate() {
        return this._startDate ? this._startDate.toDate() : null;
    }

    set endDate(endDate: Date) {
        this._endDate = mo(endDate);
    }

    get endDate() {
        return this._endDate ? this._endDate.toDate() : null;
    }

    public setFormattedStartDate(startDate: string, format: string) {
        let date = mo(startDate, format);
        if (!date.isValid()){
            throw "Provided start date and format is invalid";
        } 

        this._startDate = date;
    }

    public setFormattedEndDate(endDate: string, format: string) {
        let date = mo(endDate, format);
        if (!date.isValid()) {
            throw "Provided end date and format is invalid";
        }

        this._endDate = date;
    }

    get JSONString() {
        return JSON.stringify({
            id: this.id,
            name: this.name,
            description: this.description,
            isCredt: this.isCredit,
            value: this.value,
            cycleLength: this.cycleLength,
            startDate: mo(this.startDate).format('YYYY-MM-DD'),
            endDate: mo(this.endDate).format('YYYY-MM-DD')
        });
    }
}