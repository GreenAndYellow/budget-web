import * as mo from 'moment';
import { BudgetBalanceModel } from './budget_balance.model';
import { DateRangeModel } from './date_range.model'
import { Serializable } from '../_interfaces';

// interface BudgetBalanceModel {
//     budget: number;
//     credit: number;
//     debit: number;
// }

export class BudgetSummaryModel implements Serializable {
    private _startDate: Date = new Date();
    private _endDate: Date = new Date();
    private _cycleLength: number = 7;
    private _budget: number = 0;
    private _credit: number = 0;
    private _debit: number = 0;

    constructor(init?: any) {
        if (init) {
            Object.assign(this, init);
        }
    }

    get JSONString() {
        return JSON.stringify({
            startDate: this._startDate,
            endDate: this._endDate,
            cycleLength: this._cycleLength,
            budget: this._budget,
            credit: this._credit,
            debit: this._debit
        });
    }

    incrementDate() {
        this._startDate.setDate(this._startDate.getDate() + this._cycleLength);
        this._endDate.setDate(this._endDate.getDate() + this._cycleLength);
    }

    decrementDate() {
        this._startDate.setDate(this._startDate.getDate() - this._cycleLength);
        this._endDate.setDate(this._endDate.getDate() - this._cycleLength);
    }

    get balance() : number {
        return ((this._budget + this._credit) - this._debit);
    }

    get dateRange() : DateRangeModel {
        return new DateRangeModel(this._startDate, this._endDate);
    }

    get startDate() {
        return this._startDate;
    }

    get endDate() {
        return this._endDate;
    }

    get stringFormattedDateRange() {
        return {
            startDate: mo(this._startDate).format("DD/MM/YYYY"),
            endDate: mo(this._endDate).format("DD/MM/YYYY")
        };
    }

    set budgetBalance(balance: BudgetBalanceModel) {
        this._credit = balance.credit;
        this._debit = balance.debit;
        this._budget = balance.budget;
    }

    get credit() {
        return this._credit;
    }

    set credit(credit: number) {
        this._credit = credit;
    }

    get debit() {
        return this._debit;
    }

    set debit(debit: number) {
        this._debit = debit;
    }

    get budget() {
        return this._budget;
    }

    set budget(budget: number) {
        this._budget = budget;
    }

    set dateRange(dateRange: DateRangeModel) {
        this._startDate = dateRange.fromDate;
        this._endDate = dateRange.toDate;
    }

    set cycleLength(length: number) {
        this._cycleLength = length;
    }
}