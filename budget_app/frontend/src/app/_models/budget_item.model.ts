import * as mo from 'moment';
import { CommonBudgetInfo } from './common_budget_info.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

export class BudgetItemModel extends CommonBudgetInfo {
    // Date is stored as a moment object
    private _date: mo.Moment;

    constructor(init?: any) {
        // Convert the database format of string to DD/MM/YYYY format
        super(init);
        if (init) {
            var itemDate = mo(init.date);

            if (!itemDate.isValid()) {
                throw "Date provided to BudgetItemModel is invalid";
            }

            init.date = itemDate;
            Object.assign(this, init);
        }
    }

    get date() {
        return this._date;
    }

    set date(date: mo.Moment) {
        this._date = date;
    }

    get formattedDate(): any {
        if (this._date) {
            return this._date.format("DD/MM/YYYY");
        }
    }

    set formattedDate(date: any) {
        this._date = mo(date, "DD/MM/YYYY");
    }

    get JSONFormattedDate(): any {
        if (this._date) {
            return this._date.format("YYYY-MM-DD");
        }
    }

    get JSONString() {
        // Format the date from the calendar into from that is expected by Django
        // var budgetItemToSerialize = new BudgetItemModel();
        
        // budgetItemToSerialize = Object.assign(budgetItemToSerialize, this);
        // budgetItemToSerialize.JSONFormattedDate = this.date.format("YYYY-MM-DD");

        return JSON.stringify({
            id: this.id,
            name: this.name,
            description: this.description,
            value: this.value,
            date: this.JSONFormattedDate
        });
    }
}