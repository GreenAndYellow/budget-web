import { BudgetItemModel } from "./budget_item.model";
import { CommonBudgetInfo } from "./common_budget_info.model";

export class BudgetItemsModel<T extends CommonBudgetInfo> {
    private _budgetItems: Array<T>;

    constructor(budgetItems?: Array<T>) {
        if (budgetItems) {
            this._budgetItems = budgetItems;
        } else {
            this._budgetItems = new Array<T>();
        }
    }

    resetBudgetItems = function() {
        this._budgetItems = new Array<T>();
    }

    get budgetItems() {
        return this._budgetItems;
    }

    set budgetItems(budgetItems: Array<T>) {
        this._budgetItems = budgetItems;
    }

    get creditBudgetItems() {
        return this._budgetItems.filter(item => item.isCredit);
    }

    get debitBudgetItems() {
        return this._budgetItems.filter(item => !item.isCredit);
    }
}