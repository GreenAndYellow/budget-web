
export class CommonBudgetInfo {
    public id: number;
    public name: string;
    public description: string;
    public isCredit: boolean;
    private _value: number;

    constructor(init?: any) {
        if (init) {
            if (init.value && init.value < 0) {
                this.isCredit = false;
            } else {
                this.isCredit = true;
            }
            this.name= init.name;
            this.description = init.description;
            this.value = init.value;
            this.id = init.id;
        }
    }

    get value() {
        return this._value;
    }

    set value(value: number) {
        if (value) {
            if (!this.isCredit) {
                this._value = 0-Math.abs(value);
            } else {
                this._value = Math.abs(value);
            }
        } else {
            this._value = 0;
        }
    }

    get displayValue() {
        if (this._value) {
            return Math.abs(this._value);
        } else {
            return null;
        }
    }
}