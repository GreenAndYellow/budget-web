import { Dictionary } from '../_utils';
import { CalendarDayOfWeek } from './calendar_day_of_week.model';
import * as mo from 'moment';


export class CalendarYearItem {
    private _year: number;
    private _months: Array<Array<CalendarDayOfWeek>>;
        
    constructor(initYear: number) {
        this._months = new Array<Array<CalendarDayOfWeek>>();

        this._year = initYear;

        // Initialise with days of the init year
        if (initYear) {
            for (var month = 0; month < 12; month++) {
                var formattedMonth = month+1;
                this._months[month] = new Array<CalendarDayOfWeek>();

                for (var day = 0; day < mo(initYear + '-' + formattedMonth, 'YYYY-MM').daysInMonth(); day++) {
                    var formattedDay = day+1;
                    var dateString = initYear + '-' + formattedMonth + '-' + formattedDay;

                    this._months[month][day] = new CalendarDayOfWeek(day, mo(dateString, 'YYYY-MM-DD').weekday());
                }
            }
        }
    }

    get year() {
        return this._year;
    }

    get months() {
        let months: Array<{ month: string, index: number }> = [];

        for (var month = 0; month < this._months.length; month++) {
            months.push({ month: mo(month+1, 'MM').format('MMMM'), index: month });
        }

        return months;
    }

    getMonthDaysFromString(month: string) {
        return this._months[parseInt(mo(month, 'MMM').format('MM'))-1];
    }

    getMonthDaysFromNum(month: number) {
        return this._months[month];
    }

}