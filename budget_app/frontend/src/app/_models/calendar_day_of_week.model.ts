import * as mo from 'moment';


export class CalendarDayOfWeek {
    _date: number;
    _day: string;

    // Assumes zero indexed constructor value
    // Convert to normal value
    constructor(date?: number, day?: number) {
        date != null ? this._date = date : null;
        day != null ? this._day = this.convertNumberToDay(day) : null;
    }

    convertNumberToDay(day: number) {
        var strDay = null;

        switch (day) {
            case 0:
                strDay = "Monday";
                break;
            case 1:
                strDay = "Tuesday";
                break;
            case 2:
                strDay = "Wednesday";
                break;
            case 3:
                strDay = "Thursday";
                break;
            case 4:
                strDay = "Friday";
                break;
            case 5:
                strDay= "Saturday";
                break;
            case 6:
                strDay = "Sunday";
                break;
        }

        return strDay;
    }

    set day(day: any) {
        this._day = this.convertNumberToDay(day);
    }

    get date() {
        return this._date;
    }

    get day() {
        return this._day;
    }

}