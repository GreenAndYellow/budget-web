import { Observable } from 'rxjs';

export interface CRUD<t> {
    create(item: t) : Observable<any>;
    update(item: t) : Observable<any>;
    getAll() : Observable<any>;
    get(id: number) : Observable<any>;
    delete(id: number) : Observable<any>;
}