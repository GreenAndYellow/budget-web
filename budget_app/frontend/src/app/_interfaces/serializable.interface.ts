
export interface Serializable {
    readonly JSONString: string
}