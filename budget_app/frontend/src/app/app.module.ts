import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpModule, XSRFStrategy, CookieXSRFStrategy } from '@angular/http';
import { AppComponent } from './app.component';
import { PageLayoutComponent } from './components/common/index';
import { TokenAuthenticationService, BudgetItemService, UserBudgetSettingService } from './_services';
import { BudgetHomeComponent, UserBudgetSettingsComponent } from './components/budget/index';
import { AddBudgetComponent, FilterBudgetItemComponent, BudgetPeriodSelectorComponent } from './components/shared_components/index';
import { routing } from './app.routing'
import { LoginComponent } from './components/login/index';
import { HomeComponent } from './components/home/index';
import { xsrfFactory } from './_factories';
import { AuthGuard } from './_guards/index';
import { TextSearchFilterPipe, EnumKeyValuesPipe } from './_pipes/index';
import { CalendarModule } from 'primeng/calendar';
import { ServicesModule } from './_services';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PageLayoutComponent,
    BudgetHomeComponent,
    AddBudgetComponent,
    FilterBudgetItemComponent,
    UserBudgetSettingsComponent,
    BudgetPeriodSelectorComponent,
    TextSearchFilterPipe,
    EnumKeyValuesPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    CalendarModule,
    BrowserAnimationsModule,
    ServicesModule
  ],
  providers: [ 
        {
            provide: XSRFStrategy,
            useFactory: xsrfFactory
        },
        AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
