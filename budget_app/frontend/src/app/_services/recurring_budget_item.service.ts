import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { RecurringBudgetItemModel } from '../_models';
import { environment } from '../../environments/environment';
import { TokenAuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import { BasicCRUDService } from '../_services/basic_crud.service';

@Injectable()
export class RecurringBudgetItemService extends BasicCRUDService<RecurringBudgetItemModel> {
    constructor(http: Http, authService: TokenAuthenticationService) {
        super(http, authService, "recurringbudgetitem/");
    }
}