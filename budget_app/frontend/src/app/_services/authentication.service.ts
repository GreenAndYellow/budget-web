import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { loginDetails } from '../models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';


@Injectable()
export class TokenAuthenticationService
{
	private token: string;
	private appRoot: string;
	constructor(private http: Http, private router: Router) {
		var currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.token = currentUser && currentUser.token;
		this.appRoot = environment.baseURL;
	}

	login(loginDetails: loginDetails): Observable<boolean> {
		let headers = new Headers({
			'Content-Type': 'application/json'
		});

		return this.http.post(this.appRoot + 'get_auth_token/', JSON.stringify(loginDetails), { headers: headers }).pipe(
			map((response: Response) => {
				let token = response.json() && response.json().token;
				if (token) {
					this.token = token;
					localStorage.setItem('currentUser', JSON.stringify({ username: loginDetails.username, token: token }));
					// Indicate successful login
					return true;
				} else {
					return false;
				}
			})
		);
	}

	get userToken() {
		return JSON.parse(localStorage.getItem('currentUser'));
	}

	logout(): void {
		this.token = null;
		localStorage.removeItem('currentUser');
	}
}