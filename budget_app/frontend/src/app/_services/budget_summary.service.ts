import { Http, Headers } from '@angular/http';
import { TokenAuthenticationService } from './authentication.service';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as mo from 'moment';

@Injectable()
export class BudgetSummaryService {
    private appRoot: string;
    private headers: Headers = new Headers({
        'Authorization': 'Token ' + this.authService.userToken.token,
        'Content-Type': 'application/json'
    });

    constructor(private http: Http, private authService: TokenAuthenticationService) {
        this.appRoot = environment.baseURL + "budgetitem/summary/";
    }

    getUserBudgetSummary(startDate: Date, endDate: Date): Observable<any> {
        let formattedStartDate = mo(startDate.getTime()).format("YYYY-MM-DD");
        let formattedEndDate = mo(endDate.getTime()).format("YYYY-MM-DD");
        
        let queryParameters = "start_date=" + formattedStartDate + "&end_date=" + formattedEndDate;
        return this.http.get(this.appRoot + '?' + queryParameters, { headers: this.headers });
    }
}