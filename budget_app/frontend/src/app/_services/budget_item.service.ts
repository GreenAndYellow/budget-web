import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { BudgetItemModel } from '../_models';
import { environment } from '../../environments/environment';
import { TokenAuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BasicCRUDService } from '../_services/basic_crud.service';

import * as mo from 'moment';

@Injectable()
export class BudgetItemService extends BasicCRUDService<BudgetItemModel>
{
    constructor(http: Http, authService: TokenAuthenticationService) {
        super(http, authService, "budgetitem/");
    }

    getUserBudgetSummary(startDate: Date, endDate: Date): Observable<any> {
        let formattedStartDate = mo(startDate.getTime()).format("YYYY-MM-DD");
        let formattedEndDate = mo(endDate.getTime()).format("YYYY-MM-DD");

        let queryParameters = "start_date=" + formattedStartDate + "&end_date=" + formattedEndDate;

        return this.http.get(this.APIString + 'summary/?' + queryParameters, { headers: this.headers });
    }
}