import { NgModule } from '@angular/core';
import { TokenAuthenticationService } from './authentication.service'
import { BudgetItemService } from './budget_item.service';
import { RecurringBudgetItemService } from './recurring_budget_item.service';
import { UserBudgetSettingService } from './userbudgetsetting.service';
import { BudgetSummaryService } from './budget_summary.service';

@NgModule({
    providers: [
        TokenAuthenticationService,
        BudgetItemService,
        RecurringBudgetItemService,
        UserBudgetSettingService,
        BudgetSummaryService
    ]
})
export class ServicesModule {}