import { CRUD, Serializable } from '../_interfaces';
import { environment } from '../../environments/environment';
import { Http, Headers } from '@angular/http';
import { TokenAuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';


export class BasicCRUDService<T extends Serializable> implements CRUD<T> {
    APIString: string;
    headers: Headers = new Headers({
        'Authorization': 'Token ' + this.authService.userToken.token,
        'Content-Type': 'application/json'
    });

    constructor(public http: Http, public authService: TokenAuthenticationService, APIString: string) {
        this.APIString = environment.baseURL + APIString;
    }

    public create(object: T) : Observable<any> {
        return this.http.post(this.APIString, object.JSONString, { headers: this.headers });
    }

    public update(object: T): Observable<any> {
        return this.http.put(this.APIString, object.JSONString, { headers: this.headers });
    }

    public getAll(): Observable<any> {
        return this.http.get(this.APIString, { headers: this.headers });
    }

    public get(id: number): Observable<any> {
        return this.http.get(this.APIString + id + "/", { headers: this.headers });
    }

    public delete(id: number) : Observable<any> {
        return this.http.delete(this.APIString + id + "/", { headers: this.headers });
    }
}