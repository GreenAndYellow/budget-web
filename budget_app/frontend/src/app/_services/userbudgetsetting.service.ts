import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { UserBudgetSettingModel } from '../_models';
import { TokenAuthenticationService } from './authentication.service'
import { Observable } from 'rxjs';
import { BasicCRUDService } from '../_services/basic_crud.service';
import { map } from 'rxjs/operators';

@Injectable()
export class UserBudgetSettingService extends BasicCRUDService<UserBudgetSettingModel> {
    constructor(http: Http, authService: TokenAuthenticationService) {
        super(http, authService, "userbudgetsetting/");
    }
}