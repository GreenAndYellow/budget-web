import { Component, OnInit } from '@angular/core';
import { BudgetSummaryModel, UserBudgetSettingModel, BudgetBalanceModel, BudgetPeriodSelectorSettingModel, DateRangeModel } from '../../_models';
import { BudgetItemService, UserBudgetSettingService, BudgetSummaryService } from '../../_services';
import { Observable } from 'rxjs';
import * as mo from 'moment';

declare var PNotify: any;
declare var $: any;

@Component({
	selector: 'home-component',
	templateUrl: './home.component.html',
  	styleUrls: ['../../app.component.css']
})

// Show summary on home component
export class HomeComponent implements OnInit {
	private budgetSummaryModel: BudgetSummaryModel;
	private dateGroupElement: any;
	private budgetPeriodSelectorSettings: BudgetPeriodSelectorSettingModel;
	
	constructor (private budgetItemService: BudgetItemService, private budgetSummaryService: BudgetSummaryService, private userBudgetSettingService: UserBudgetSettingService) {
	}

	ngOnInit() {
		var self = this;
		this.budgetSummaryModel = new BudgetSummaryModel();
		// https://stackoverflow.com/questions/38099177/chaining-ajax-requests-angular-2-and-es2015-promises

		this.userBudgetSettingService.getAll().subscribe(budgetSettings => {
			this.budgetSummaryService.getUserBudgetSummary(this.budgetSummaryModel.startDate, this.budgetSummaryModel.endDate).subscribe(budgetSummary => {
				this.initialiseComponent(budgetSummary, budgetSettings);
			}, error => {
				new PNotify({
					title: 'Fail',
					styling: 'brighttheme',
					text: 'Error initiating user budget setting component (Failed to retrieve user budget summary)',
					type: 'error'
				})
			});
		}, error => {
			new PNotify({
				title: 'Fail',
				styling: 'brighttheme',
				text: 'Error initiating user budget setting component (Failed to retrieve user budget setting)',
				type: 'error'
			});
		});
	}

	private budgetPeriodSelectorCallback(cycleBudget) {
		return (period) => {
			this.budgetSummaryModel.dateRange = new DateRangeModel(period.fromDate, period.toDate);

			this.budgetSummaryService.getUserBudgetSummary(this.budgetSummaryModel.startDate, this.budgetSummaryModel.endDate).subscribe(
				results => {
					let userBudgetSummary = new BudgetSummaryModel(JSON.parse(results.text()));
	
					this.budgetSummaryModel.budgetBalance = new BudgetBalanceModel(userBudgetSummary.credit, userBudgetSummary.debit, cycleBudget);
	
					this.setBudgetBarPercentage(this.budgetSummaryModel.budget, this.budgetSummaryModel.balance);
				}
			);
		}
	}

	private initialiseBudgetPeriodSelector(userBudgetSetting, budgetPeriodCallback) {
		return new BudgetPeriodSelectorSettingModel({
			userBudgetSetting: new UserBudgetSettingModel(userBudgetSetting),
			parentElement: $("#date-group"),
			callback: budgetPeriodCallback
		});
	}

	private initialiseComponent(budgetSummary, userBudgetSettings) {
		let userBudgetSummary = JSON.parse(budgetSummary.text());
		let currUserBudgetSetting = JSON.parse(userBudgetSettings.text())[0];

		let userCyclePeriod = this.getCycleDateRange(currUserBudgetSetting);
		let userCycleBudget = currUserBudgetSetting.cycleBudget;

		this.budgetSummaryModel.dateRange = new DateRangeModel(userCyclePeriod.cycleStart, userCyclePeriod.cycleEnd);
		this.budgetSummaryModel.budgetBalance = new BudgetBalanceModel(userBudgetSummary.credit, userBudgetSummary.debit, userCycleBudget);
		this.setBudgetBarPercentage(this.budgetSummaryModel.budget, this.budgetSummaryModel.balance);
		this.budgetPeriodSelectorSettings = this.initialiseBudgetPeriodSelector(currUserBudgetSetting, this.budgetPeriodSelectorCallback(userCycleBudget));
		this.budgetSummaryModel.cycleLength = this.budgetPeriodSelectorSettings.userBudgetSetting.cycleLength;
	}

	private getCycleDateRange(userBudgetSetting: UserBudgetSettingModel) {
		let cycleStartDate = mo(userBudgetSetting.cycleStartDate);
		let cycleDayNumber = mo().diff(cycleStartDate, 'days') % userBudgetSetting.cycleLength;

		let currentCycleEndDate = mo().add(userBudgetSetting.cycleLength-cycleDayNumber, 'days');
		let currentCycleStartDate = mo(currentCycleEndDate).subtract(userBudgetSetting.cycleLength, 'days');

		return {
			cycleStart: currentCycleStartDate.toDate(),
			cycleEnd: currentCycleEndDate.toDate()
		}
	}

	// Calculate bar percentage from budget value
	setBudgetBarPercentage(budget: number, balance: number) {
		if (balance < 0) {
			$('.progress-bar').css({'background': 'red', 'width': '100%'});
		} else {
			let budgetPercentage = Math.round(balance/budget)*100;
			$('.progress-bar').css({'background': 'rgba(11, 14, 216, 0.699)', 'width': '' + budgetPercentage + '%'})
		}
	}

	incrementDate() {
		this.budgetSummaryModel.incrementDate();
	}

	decrementDate() {
		this.budgetSummaryModel.decrementDate();
	}
}