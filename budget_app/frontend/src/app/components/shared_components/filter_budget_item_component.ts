import { Component, OnInit, Input } from '@angular/core';
import { BudgetItemModel, FilterBudgetItemModel, BudgetItemsModel } from '../../_models/index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

declare var $: any;

@Component({
    selector: "filter-budget-item",
    templateUrl: "./filter_budget_item_component.html",
    styleUrls: ['../../app.component.css']
})
export class FilterBudgetItemComponent implements OnInit {
    @Input() userBudgetItems: BudgetItemsModel;
    @Input() parentCallback: Subject<Function>;
    filterBudgetForm: FormGroup;
    filterBudgetItem: FilterBudgetItemModel;
    filteredUserBudgetItems: BudgetItemsModel;

    constructor() {
        this.filterBudgetItem = new FilterBudgetItemModel();
        this.filteredUserBudgetItems = new BudgetItemsModel();
    }

    ngOnInit() {
        let self = this;

        this.filterBudgetForm = new FormGroup({
            'budgetItemFromDate': new FormControl(this.filterBudgetItem.formattedBudgetDateFrom, []),
            'budgetItemToDate': new FormControl(this.filterBudgetItem.formattedBudgetDateTo, [])
        });

        this.filterBudgetForm.get('budgetItemFromDate').valueChanges.subscribe((fromDate) => {
            this.filterBudgetItem.setFormattedBudgetDateFrom(fromDate);
        });

        this.filterBudgetForm.get("budgetItemToDate").valueChanges.subscribe((toDate) => {
            this.filterBudgetItem.setFormattedBudgetDateTo(toDate);
        });

        this.parentCallback.subscribe(callback => {
            this.filteredUserBudgetItems.budgetItems = this.userBudgetItems.budgetItems.filter(function (userBudgetItem, index) {
                return self.filterBudgetItem.passesFilter(userBudgetItem);
            });

            callback(this.filteredUserBudgetItems);
        });
    }

    filterBudgetItems = function () {
        console.log(this.userBudgetItems);
    }
}