import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Dictionary } from '../../_utils';
import { CalendarDayOfWeek, CalendarYearItem, DateItemModel, UserBudgetSettingModel, DateRangeModel, BudgetPeriodSelectorSettingModel } from '../../_models'
import * as mo from 'moment';

declare var $: any;

@Component({
    selector: 'budget-period-selector-component',
    templateUrl: './budget_period_selector.component.html',
    styleUrls: ['../../app.component.css']
})
export class BudgetPeriodSelectorComponent implements OnInit {
    // jQuery object which is the parent element that the period selector will be displayed underneath
    // @Input() parentElement?: any;
    // @Input() initYear?: number;
    // @Input() userBudgetSetting?: UserBudgetSettingModel;
    // @Input() pageLength?: number;
    // @Input() callback?: any;

    @Input() settingsObject: BudgetPeriodSelectorSettingModel;

    // Store function for use in ngFor loop
    objectKeys = Object.keys;
    periodStartYear: number; 
    dateDictionary: Dictionary<CalendarYearItem>;
    selectedDate: DateItemModel;
    dateRangeList: Array<Array<DateRangeModel>> = new Array<Array<DateRangeModel>>();
    filteredDateRangeList: Array<Array<DateRangeModel>> = new Array<Array<DateRangeModel>>();

    private _initYear: number;
    private _pageLength: number;
    private _currPage: number;

    constructor() {
    }

    // https://codepen.io/wallaceerick/pen/ctsCz

    // http://jsbin.com/hokivosoca/edit?html,css,output

    ngOnInit() {
        let now = new Date();

        this._initYear = this.settingsObject.initYear ? this.settingsObject.initYear : 1994;
        this._pageLength = this.settingsObject.pageLength ? this.settingsObject.pageLength : 6;

        this.dateDictionary = new Dictionary<CalendarYearItem>();
        this.selectedDate = new DateItemModel(this._initYear);

        if (this.settingsObject.parentElement) {
            $("#period-selector").position({
                my: "top+12",
                at: "",
                of: this.settingsObject.parentElement,
                collision: "fit"
            }).hide();

            this.settingsObject.parentElement.on('click', function () {
                $("#period-selector").toggle();
            });


            for (var year = this._initYear; year <= now.getFullYear(); year++) {
                this.dateDictionary.add(year.toString(), new CalendarYearItem(year));
            }

            this.selectedDate.setDate(null, null, null);
        }

        if (this.settingsObject.userBudgetSetting) {
            this.constructDefaultPeriodArray();
        }
    }

    private inRange(dateRange: DateRangeModel, checkDate: DateItemModel): boolean {
        let yearInRange = (
            (checkDate.year == null) ||
            (checkDate.year >= dateRange.fromDate.getFullYear() 
            && checkDate.year <= dateRange.toDate.getFullYear())
        );

        let monthInRange = (
            (checkDate.month == null) ||
            (checkDate.month >= dateRange.fromDate.getMonth()
            && checkDate.month <= dateRange.toDate.getMonth())
        );

        let dayInRange = (
            (checkDate.day == null) ||
            (checkDate.day >= dateRange.fromDate.getDay()
            && checkDate.day <= dateRange.toDate.getDay())
        );

        return (yearInRange && monthInRange && dayInRange);
    }

    private constructPeriodArray(periods: Array<DateRangeModel>): Array<Array<DateRangeModel>> {
        let paginatedArray = new Array<Array<DateRangeModel>>();
        let itemPageCount = this._pageLength;
        let pageIndex = 0;

        for (let i = 0; i < periods.length; i++) {
            if (!paginatedArray[pageIndex]) {
                paginatedArray[pageIndex] = new Array<DateRangeModel>();
            }

            if (itemPageCount > 0) {
                paginatedArray[pageIndex].push(periods[i]);
                itemPageCount -= 1;
            } else {
                itemPageCount = this._pageLength;
                pageIndex += 1;
            }
        }

        return paginatedArray;
    }

    constructDefaultPeriodArray(): void {
        let userBudgetSetting = this.settingsObject.userBudgetSetting;

        // Start date on current date
        let itemPageCount = this._pageLength;
        let pageIndex = 0;

        let periods = new Array<DateRangeModel>();

        // Get modulo date difference between period start date and current date, gives how many days into current cycle
        let cycleStartDate = mo(userBudgetSetting.cycleStartDate);
        let cycleDayNumber = mo().diff(cycleStartDate, 'days') % userBudgetSetting.cycleLength;
        let iterDate = mo().add(userBudgetSetting.cycleLength-cycleDayNumber, 'days');

        while (iterDate.year() > (this._initYear - 1)) {
            let endRange = iterDate.toDate();
            iterDate.subtract(userBudgetSetting.cycleLength, 'd');
            let startRange = mo(iterDate).toDate();

            periods.push(new DateRangeModel(startRange, endRange));
        }
        
        periods.reverse();
        this.dateRangeList = this.constructPeriodArray(periods);
        this.filteredDateRangeList = this.dateRangeList;

        if (this.dateRangeList.length > 0) {
            this._currPage = 0; 
        }
    }

    filterPeriodArray($event): void {
        if ($event.target.id == "period-selector-year") {
            this.selectedDate.month = null;
            this.selectedDate.day = null;
        } else if ($event.target.id == "period-selector-month") {
            this.selectedDate.day = null;
        }

        this.filteredDateRangeList = new Array<Array<DateRangeModel>>();
        let periods = new Array<DateRangeModel>();

        if (!this.selectedDate.year) {
            this.filteredDateRangeList = this.dateRangeList;
        } else {
            let selectedDate = this.selectedDate;
            const self = this;
            
            $.grep(this.dateRangeList, function (dateRangePage, i) {
                for (let j = 0; j < dateRangePage.length; j++) {
                    if (self.inRange(dateRangePage[j], self.selectedDate)) {
                        periods.push(dateRangePage[j]);
                    }
                }
            });
        }

        this.filteredDateRangeList = this.constructPeriodArray(periods);
    }

    changePage(change: number): void {
        let newIndex = this._currPage + change;

        if (!(newIndex < 0 || newIndex >= this.filteredDateRangeList.length)) {
            this._currPage = newIndex;
        }
    }

    selectPeriod(period: DateRangeModel): void {
        $("#period-selector").hide();
        this.settingsObject.callback(period);
    }
} 



