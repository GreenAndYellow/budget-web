import { Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { BudgetItemModel, RecurringBudgetItemModel } from '../../_models/index';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;

@Component({
    selector: 'add-budget',
    templateUrl: "./add_budget.component.html",
    styleUrls: ['../../app.component.css']
})
export class AddBudgetComponent implements OnInit {
    budgetItem: BudgetItemModel;
    recurringBudgetItem: RecurringBudgetItemModel;
    budgetForm: FormGroup;
    recurringBudgetForm: FormGroup;
    formSubmitted: boolean = false;
    @Input() parentCallback: Subject<Function>;
    @Input() isCredit: boolean;
    @Input() budgetItemType?: string;

    ngOnChanges(changes: SimpleChanges) {
        let isCredit = changes.isCredit ? changes.isCredit.currentValue : null;
        if (isCredit) {
            if (this.isRecurring) {
                this.recurringBudgetItem.isCredit = isCredit;
            } else {
                this.budgetItem.isCredit = isCredit;
            }
        }
    }

    ngOnInit() {
        $("#item-date").datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true
        });

        this.budgetItem = new BudgetItemModel();
        this.recurringBudgetItem = new RecurringBudgetItemModel();

        this.parentCallback.subscribe(callback => {
            let form = this.isRecurring ? this.recurringBudgetForm : this.budgetForm;
            let item = this.isRecurring ? this.recurringBudgetItem : this.budgetItem;

            // If the budget item is valid return the budget item otherwise null
            if (form.invalid) {
                console.log(form.errors);
                this.formSubmitted = true;
            } else {
                callback(item);
                
                // Reset form
                // form.reset();
                // this.budgetItem = new BudgetItemModel();
                // this.recurringBudgetItem = new RecurringBudgetItemModel();
            }
            
        });

        this.budgetForm = new FormGroup({
            'name': new FormControl(this.budgetItem.name, [
                Validators.required
            ]),
            'description': new FormControl(this.budgetItem.description),
            'value': new FormControl(this.budgetItem.value, [
                Validators.pattern("^[\\+\\-]?[0-9]+(\\.[0-9]+)?$"),
                Validators.required,
                Validators.min(0)
            ]),
            'date': new FormControl(this.budgetItem.formattedDate, [
                Validators.required
            ])
        });

        this.recurringBudgetForm = new FormGroup({
            'name': new FormControl(this.recurringBudgetItem.name, [
                Validators.required
            ]),
            'description': new FormControl(this.recurringBudgetItem.description),
            'value': new FormControl(this.recurringBudgetItem.value, [
                Validators.pattern("^[\\+\\-]?[0-9]+(\\.[0-9]+)?$"),
                Validators.required,
                Validators.min(0)
            ]),
            'fromDate': new FormControl(this.recurringBudgetItem.startDate, [
                Validators.required
            ]),
            'toDate': new FormControl(this.recurringBudgetItem.endDate, [
                Validators.required
            ]),
            'cycleLength': new FormControl(this.recurringBudgetItem.cycleLength, [
                Validators.required
            ])
        });

        // Form value subscriptions to update underlying model

        // Budget
        this.budgetForm.get("name").valueChanges.subscribe((name) => {
            this.budgetItem.name = name;
        });

        this.budgetForm.get("description").valueChanges.subscribe((description) => {
            this.budgetItem.description = description;
        });

        this.budgetForm.get("value").valueChanges.subscribe((value) => {
            this.budgetItem.value = value;
        });

        this.budgetForm.get("date").valueChanges.subscribe((date) => {
            this.budgetItem.date = date;
        });

        // Recurring Budget
        this.recurringBudgetForm.get("name").valueChanges.subscribe((name) => {
            this.recurringBudgetItem.name = name;
        });

        this.recurringBudgetForm.get("description").valueChanges.subscribe((description) => {
            this.recurringBudgetItem.description = description;
        });

        this.recurringBudgetForm.get("value").valueChanges.subscribe((value) => {
            this.recurringBudgetItem.value = value;
        });

        this.recurringBudgetForm.get("fromDate").valueChanges.subscribe((fromDate) => {
            this.recurringBudgetItem.startDate = fromDate;
        });

        this.recurringBudgetForm.get("toDate").valueChanges.subscribe((toDate) => {
            this.recurringBudgetItem.endDate = toDate;
        });

        this.recurringBudgetForm.get("cycleLength").valueChanges.subscribe((cycleLength) => {
            this.recurringBudgetItem.cycleLength = cycleLength;
        });
    }

    get isRecurring() {
        return this.budgetItemType && this.budgetItemType=="recurring";
    }

    // get budgetName() { return this.budgetForm.get('name') };
    // get budgetDescription() { return this.budgetForm.get('description') };
    // get budgetValue() { return this.budgetForm.get('value') };
    // get budgetDate() { return this.budgetForm.get('date') };

    recurringBudgetFormField(field: string) {
        return this.recurringBudgetForm.get(field);
    }

    budgetFormField(field: string) {
        return this.budgetForm.get(field);
    }

    resetComponent() {
        this.budgetForm.reset();
        this.recurringBudgetForm.reset();
        this.recurringBudgetItem = new RecurringBudgetItemModel();
        this.budgetItem = new BudgetItemModel();
    }
}