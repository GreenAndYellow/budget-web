import { Component, OnInit, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http, Response, RequestOptions, Headers } from '@angular/http'
import { loginDetails, authenticatedSessionDetails } from '../../models'
import { TokenAuthenticationService } from '../../_services'
import { Router } from '@angular/router'

@Component({
	templateUrl: './login.component.html',
  	styleUrls: ['../../app.component.css']
})
export class LoginComponent implements OnInit {
	appRoot;
	getAuthURL = "get_auth_token";
	loginDetails =  new loginDetails();
  	validLogin = true;

	constructor(private router: Router, private http: Http, private authService: TokenAuthenticationService) 
	{
	}

	ngOnInit() {
		this.authService.logout();
	}

  	submitLogin = function () {
  		this.authService.login(this.loginDetails).subscribe(result => {
  			if (result) {
  				this.router.navigate(['']);
  			} else {
  				this.validLogin = false;
  			}
  		},
  		err => {
  			this.validLogin = false;
  		});
  	}



}