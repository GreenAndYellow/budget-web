import { Component, AfterViewInit, OnInit } from '@angular/core';
import { TokenAuthenticationService } from '../../_services';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
declare var $: any;

@Component({
	selector: 'app-template-layout',
	templateUrl: './page_layout.component.html',
  	styleUrls: ['../../app.component.css',]
})

// https://github.com/BlackrockDigital/startbootstrap-simple-sidebar
// https://angular.io/guide/router#the-sample-application
export class PageLayoutComponent implements AfterViewInit, OnInit {
	constructor (private router: Router, private http: Http, private authService: TokenAuthenticationService) {
    }

    ngAfterViewInit() {
	    $("#menu-toggle").click(function(e) {
        	e.preventDefault();
        	$("#wrapper").toggleClass("toggled");
		});		
		
		$("#menu-close").click(function (e) {
			e.preventDefault();
			$("#wrapper").removeClass("toggled");
		});

    	this.router.navigate(['/home']);
    }

    ngOnInit() {
    }

	logout = function () {
		this.authService.logout();
		this.router.navigate(['/login']);
	}

	hideSidebar = function (event) {
		$("#wrapper").toggleClass("toggled");
	}
}