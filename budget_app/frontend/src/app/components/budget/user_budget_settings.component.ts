import { Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { DaysOfWeek } from '../../_enums';
import { UserBudgetSettingModel } from '../../_models';
import { UserBudgetSettingService } from '../../_services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { conditionalIfValidator, checkIfValidDateFunction } from '../../_utils';

declare var PNotify: any;
declare var $: any;

@Component({
    selector: 'user-budget-settings',
    templateUrl: './user_budget_settings.component.html',
    styleUrls: ['../../app.component.css']
})
export class UserBudgetSettingsComponent implements OnInit {
    daysOfWeek = DaysOfWeek;
    dayOfWeekKeys: any[];
    userBudgetSetting: UserBudgetSettingModel;
    userBudgetSettingForm: FormGroup;

    constructor(private userBudgetSettingService: UserBudgetSettingService) {
        var self = this;

        this.userBudgetSetting = new UserBudgetSettingModel();
        this.userBudgetSettingForm = new FormGroup({
            'cycleStartDate': new FormControl('', [Validators.required, conditionalIfValidator(checkIfValidDateFunction("DD/MM/YYYY"))]),
            'cycleLength': new FormControl('', [Validators.required, Validators.min(1)]),
            'cycleBudget': new FormControl('', [Validators.required, Validators.min(0)])
        });

        this.userBudgetSettingForm.get('cycleStartDate').valueChanges.subscribe((cycleStartDate) => {
            this.userBudgetSetting.cycleStartDate = cycleStartDate;
        });
        this.userBudgetSettingForm.get('cycleLength').valueChanges.subscribe((cycleLength) => {
            this.userBudgetSetting.cycleLength = cycleLength;
        });
        this.userBudgetSettingForm.get('cycleBudget').valueChanges.subscribe((cycleBudget) => {
            this.userBudgetSetting.cycleBudget = cycleBudget;
        });

        // For now set the user budget setting to the existing user budget setting if it exists
        // Assume each user has only one user budget setting
        this.userBudgetSettingService.getAll().subscribe(
            result => {
                var userBudgetSettings = JSON.parse(result._body);
                if (userBudgetSettings.length == 0) {
                    this.userBudgetSetting = new UserBudgetSettingModel();
                } else {
                    this.userBudgetSetting = new UserBudgetSettingModel(userBudgetSettings[0]);
                }

                this.userBudgetSettingForm.setValue({
                    'cycleStartDate': this.userBudgetSetting.cycleStartDate,
                    'cycleLength': this.userBudgetSetting.cycleLength,
                    'cycleBudget': this.userBudgetSetting.cycleBudget
                });
            },
            error => {
                console.log("Error getting user budget settings");
            }
        )
    }

    ngOnInit() {
    }

    saveUserBudgetSetting(formValues: FormGroup) {
        if (formValues.valid) {
            if (this.userBudgetSetting.id > 0) {
                this.updateUserBudgetSetting();
            } else {
                this.createUserBudgetSetting();
            }
        }
    }

    private createUserBudgetSetting () {
        this.userBudgetSettingService.create(this.userBudgetSetting).subscribe(
            result => {
                let createResult = JSON.parse(result._body);
                this.userBudgetSetting = new UserBudgetSettingModel(createResult);

                new PNotify({
                    title: 'Success',
                    styling: 'brighttheme',
                    text: 'Successfully created user budget setting',
                    type: 'success'
                });
            },
            error => {
                new PNotify({
                    title: 'Fail',
                    styling: 'brighttheme',
                    text: 'Error creating user budget setting',
                    type: 'error'
                });
            }
        )
    }

    private updateUserBudgetSetting() {
        this.userBudgetSettingService.update(this.userBudgetSetting).subscribe(
            result => {
                new PNotify({
                    title: 'Success',
                    styling: 'brighttheme',
                    text: 'Successfully updated user budget settings',
                    type: 'success'
                });
            },
            error => {
                new PNotify({
                    title: 'Fail',
                    styling: 'brighttheme',
                    text: 'Error updating user budget settings',
                    type: 'error'
                });
            }
        )
    }

    get cycleLengthControl() {
        return this.userBudgetSettingForm.controls['cycleLength'];
    }

    get cycleBudgetControl() {
        return this.userBudgetSettingForm.controls['cycleBudget'];
    }

    get cycleStartDateControl() {
        return this.userBudgetSettingForm.controls['cycleStartDate'];
    }

}