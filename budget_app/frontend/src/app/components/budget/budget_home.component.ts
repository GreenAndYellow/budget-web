import { Component, OnInit, ViewChild } from '@angular/core';
import { BudgetItemModel, FilterBudgetItemModel, BudgetItemsModel, RecurringBudgetItemModel, CommonBudgetInfo } from '../../_models/index';
import { Subject } from 'rxjs';
import { BudgetItemService, RecurringBudgetItemService } from '../../_services/index';
import { Http } from '@angular/http';
import { AddBudgetComponent } from '../shared_components/add_budget.component'

declare var $: any;
declare var PNotify: any;

@Component({
    selector: "budget-home",
    templateUrl: "./budget_home.component.html",
    styleUrls: ['../../app.component.css']
})
export class BudgetHomeComponent implements OnInit {
    filteredUserBudgetItems: BudgetItemsModel<BudgetItemModel>;
    filteredRecurringBudgetItems: BudgetItemsModel<RecurringBudgetItemModel>;

    userBudgetItems: BudgetItemsModel<BudgetItemModel>;
    recurringUserBudgetItems: BudgetItemsModel<RecurringBudgetItemModel>;
    budgetItemsFilter: FilterBudgetItemModel;
    
    addBudgetCallback: Subject<Function> = new Subject();

    addRecurringBudgetCallback: Subject<Function> = new Subject();
    filterBudgetItemsCallback: Subject<Function> = new Subject();

    @ViewChild('addBudgetItemComponent')
    private addBudgetItemComponent: AddBudgetComponent;

    // If the budget item to be added is a credit item or a debit item
    budgetItemIsCredit: boolean;
    budgetItemType: string;

    ngOnInit() {
        this.filteredUserBudgetItems = new BudgetItemsModel<BudgetItemModel>();
        this.filteredRecurringBudgetItems = new BudgetItemsModel<RecurringBudgetItemModel>();
        this.userBudgetItems = new BudgetItemsModel<BudgetItemModel>();
        this.recurringUserBudgetItems = new BudgetItemsModel<RecurringBudgetItemModel>();

        this.budgetItemService.getAll().subscribe(
            result => {
                let budgetItems = JSON.parse(result._body);
                // Sort budget items by date
                // budgetItems.Sort((b1, b2) => {

                // });

                budgetItems.forEach(item => {
                    this.userBudgetItems.budgetItems.push(new BudgetItemModel(item));
                    this.filteredUserBudgetItems.budgetItems.push(new BudgetItemModel(item));
                });
            },
            err => {
                console.log("error", err)
            }
        );

        this.recurringBudgetItemService.getAll().subscribe(
            result => {
                let budgetItems = JSON.parse(result._body);

                budgetItems.forEach(item => {
                    this.recurringUserBudgetItems.budgetItems.push(new RecurringBudgetItemModel(item));
                    this.filteredRecurringBudgetItems.budgetItems.push(new RecurringBudgetItemModel(item));
                });

                console.log(budgetItems);
                console.log(this.filteredRecurringBudgetItems.creditBudgetItems);
            },
            err => {
                console.log("error", err);
            }

        );

        $("#addBudgetItemModal").on('hidden.bs.modal', () => {
            this.addBudgetItemComponent.resetComponent();
        });
    }

    constructor (private budgetItemService: BudgetItemService, private recurringBudgetItemService: RecurringBudgetItemService) {
        this.budgetItemsFilter = new FilterBudgetItemModel();
    }

    get isRecurring() {
        return this.budgetItemType == "recurring";
    }

    refreshUserBudgetItems = function () {
        return this.budgetItemService.getAll().subscribe(
            result => {
                let budgetItems = JSON.parse(result._body);
                // Sort budget items by date
                // budgetItems.Sort((b1, b2) => {

                // });
                // Need to rerun filter

                this.userBudgetItems.resetBudgetItems();
                budgetItems.forEach(item => {
                    this.userBudgetItems.budgetItems.push(new BudgetItemModel(item));
                    this.filteredUserBudgetItems.budgetItems = this.userBudgetItems.budgetItems;
                });
            },
            err => {
                console.log("Error reloading user budget items", err)
            }
        );
    }

    openAddBudgetItemModal = function (isCredit: boolean) {
        this.budgetItemIsCredit = isCredit;
        this.budgetItemIsRecurring = false;
        this.budgetItemType = "single";
        $("#addBudgetItemModal").modal("show");
        return false;
    }

    openRecurringBudgetItemModal = function (isCredit: boolean) {
        this.budgetItemIsCredit = isCredit;
        this.budgetItemIsRecurring = true;
        this.budgetItemType = "recurring";
        $("#addBudgetItemModal").modal("show");
        return false;
    }

    private createBudgetItem = (budgetItem) => {
        if (this.isRecurring) {
            this.recurringBudgetItemService.create(budgetItem).subscribe(
                result => {
                    console.log("success", result);
                    new PNotify({
                        title: 'Success',
                        styling: 'brighttheme',
                        text: 'Successfully created recurring budget item',
                        type: 'success'
                    });
    
                    this.refreshUserBudgetItems();
                    $('#addRecurringBudetItemModal').modal('hide');
                },
                err => {
                    new PNotify({
                        title: 'Fail',
                        styling: 'brighttheme',
                        text: 'Failed to create recurring budget item',
                        type: 'fail'
                    })
                    console.log("error", err);
                }
            );
        } else {
            this.budgetItemService.create(budgetItem).subscribe(
                result => {
                    console.log("success", result);
                    new PNotify({
                        title: 'Success',
                        styling: 'brighttheme',
                        text: 'Successfully created budget item',
                        type: 'success'
                    })
    
                    this.refreshUserBudgetItems();
                    $('#addBudgetItemModal').modal('hide');
                },
                err => {
                    new PNotify({
                        title: 'Fail',
                        styling: 'brighttheme',
                        text: 'Failed to create budget item',
                        type: 'fail'
                    })
    
                    console.log("error", err);
                }
            );
        }
    }

    // private createRecurringBudgetItem = (recurringItem) => {
    //     console.log("here");
    //     this.recurringBudgetItemService.create(recurringItem).subscribe(
    //         result => {
    //             console.log("success", result);
    //             new PNotify({
    //                 title: 'Success',
    //                 styling: 'brighttheme',
    //                 text: 'Successfully created recurring budget item',
    //                 type: 'success'
    //             });

    //             this.refreshUserBudgetItems();
    //             $('#addRecurringBudetItemModal').modal('hide');
    //         },
    //         err => {
    //             new PNotify({
    //                 title: 'Fail',
    //                 styling: 'brighttheme',
    //                 text: 'Failed to create recurring budget item',
    //                 type: 'fail'
    //             })

    //             console.log("error", err);
    //         }
    //     );
    // }

    addBudgetItem = function () {
        this.addBudgetCallback.next(this.createBudgetItem)
    }

    // addRecurringBudgetItem = function () {
    //     this.addRecurringBudgetCallback.next(this.createRecurringBudgetItem);
    // }

    deleteBudgetItem = function (budgetItem: BudgetItemModel) {
        this.budgetItemService.delete(budgetItem.id).subscribe(
            result => {
                console.log("success", result);

                new PNotify({
                    title: 'Success',
                    styling: 'brighttheme',
                    text: 'Successfully delete budget item',
                    type: 'success'
                });

                this.refreshUserBudgetItems();
            },
            err => {
                new PNotify({
                    title: 'Fail',
                    styling: 'brighttheme',
                    text: 'Failed to delete budget item',
                    type: 'fail'
                })

                console.log("error", err);
            }
        );
    }

    filterBudgetItems = function () {
        // this.filterBudgetItemsCallback.next(function (filteredUserBudgetItems: BudgetItemsModel) {
        //     $('#filterBudgetItemModal').modal('hide');
        //     self.filteredUserBudgetItems.budgetItems = filteredUserBudgetItems.budgetItems;
        // });

        this.filterBudgetItemsCallback.next((filteredUserBudgetItems: BudgetItemsModel<BudgetItemModel>) => {
            $('#filterBudgetItemModal').modal('hide');
            this.filteredUserBudgetItems.budgetItems = filteredUserBudgetItems.budgetItems;
        });
    }

    quickSearchBudgetItems = function (searchTerm) {
        this.budgetItemsFilter.searchField = searchTerm;
        this.filteredUserBudgetItems.budgetItems = this.userBudgetItems.budgetItems.filter(budgetItem => this.budgetItemsFilter.passesQuickSearch(budgetItem));
    }
}

