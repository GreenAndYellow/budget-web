export class loginDetails {
	username: string;
    password: string;

	constructor() 
	{
		this.username = ""
		this.password = ""
	}
}

export class authenticatedSessionDetails {
	username: string;
	token: string;

	constructor()
	{
		this.username = ""
		this.token = ""
	}
}
