import { BudgetPage } from './app.po';

describe('budget App', () => {
  let page: BudgetPage;

  beforeEach(() => {
    page = new BudgetPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
