requirejs.config({
	baseUrl: "/static/budget/js",
	paths: {
		"bootstrapjs": "./bootstrap/bootstrap",
		"jquery": "./jquery/jquery-3.2.1",
		"angular": "./angular/angular"
	}
});