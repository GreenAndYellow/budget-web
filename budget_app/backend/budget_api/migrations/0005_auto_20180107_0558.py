# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-07 05:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('budget_api', '0004_auto_20171229_1003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userbudgetsetting',
            name='cycleStartDate',
            field=models.DateField(null=True),
        ),
    ]
