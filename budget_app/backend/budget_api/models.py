from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token

# Create your models here.
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class CommonBudgetInfo(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    value = models.FloatField()

    class Meta:
        abstract = True

class BudgetItem(CommonBudgetInfo):
    date = models.DateField(null=False)
    user = models.ForeignKey(User, related_name='budget_item_user', null=False, default=-1)

class RecurringBudgetItem(CommonBudgetInfo):
    startDate = models.DateField(null = False)
    endDate = models.DateField()
    cycleLength = models.IntegerField()
    user = models.ForeignKey(User, related_name='recurring_budget_item_user', null=False, default=-1)

class UserBudgetSetting(models.Model):
    cycleLength = models.IntegerField()
    cycleBudget = models.FloatField()
    cycleStartDay = models.CharField(max_length=9)
    cycleStartDate = models.DateField(null = False)
    user = models.ForeignKey(User, related_name='user_budget_setting_user', null=False, default=-1)

class UserBudgetSummary(object):
    def __init__(self, credit = None, debit = None):
        self.credit = credit or 0
        self.debit = debit or 0

