from django.core import serializers
from django.test import TestCase
from rest_framework.test import APIRequestFactory, APITestCase, force_authenticate, APIClient
from budget_api.models import BudgetItem
from django.contrib.auth.models import User
from budget_api.views import BudgetItemView
from budget_api.serializers import BudgetItemSerializer
from django.urls import reverse
from rest_framework.authtoken.models import Token
import datetime

# Create your tests here.
class BudgetItemTestCases(APITestCase):
    def setUp(self):
        self.view = BudgetItemView.as_view()
        self.factory = APIRequestFactory()

        # Setup 2 users to test creation of models for 2 accounts in parallel
        self.user = User(username="test")
        password = "testing"
        self.user.set_password(password)
        self.user.save()
        self.created_user = User.objects.get(username="test")

        self.user2 = User(username="test 2")
        password = "testing"
        self.user2.set_password(password)
        self.user2.save()
        self.created_user_2 = User.objects.get(username="test 2")

        token = Token.objects.get(user_id=self.created_user.id)
        token2 = Token.objects.get(user_id=self.created_user_2.id)

        self.client = APIClient()
        self.client2 = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        self.client2.credentials(HTTP_AUTHORIZATION="Token " + token2.key)


    # Test creating and deleting budget items for 2 users in parallel
    def test_simple_bugetitem_creation_deletion(self):
        # Create budget_item using BudgetItemCreateRetrieve view first
        budget_item_CRUD_url = reverse('budget_item_CRUD')

        # Step 1: Create user budget items
        create_data = { 'name': 'abc', 'date': '2007-01-03', 'description': 'cba', 'value': 213.1 }
        create_response = self.client.post(budget_item_CRUD_url, create_data)
        create_response_data = create_response.data
        self.assertEqual(create_response.status_code, 201)

        create_data_2 = { 'name': 'abc', 'date': '2004-03-03', 'description': 'cba', 'value': 1213.1 }
        create_response2 = self.client2.post(budget_item_CRUD_url, create_data_2)
        create_response_data2 = create_response2.data
        self.assertEqual(create_response.status_code, 201)

        # Step 2: Retrieve the budget_item using BudgetItemCreateRetrieve view and check it is correct
        budget_item_retrieve_url = "/budgetitem/" + str(create_response.data['id']) + "/"
        budget_item2_retrieve_url = "/budgetitem/" + str(create_response2.data['id']) + "/"

        get_response = self.client.get(budget_item_retrieve_url)
        get_response2 = self.client2.get(budget_item2_retrieve_url)
        get_response_data = get_response.data
        get_response_data2 = get_response2.data
        self.assertEqual(get_response.status_code, 200)
        self.assertEqual(get_response2.status_code, 200)

        for attr in create_response_data:
            self.assertTrue(create_response_data[attr] == get_response_data[attr])

        for attr in create_response_data2:
            self.assertTrue(create_response_data2[attr] == get_response_data2[attr])


        # Step 3: Get all budget items for both users and check that it matches the budget items that were created
        get_list_response = self.client.get("/budgetitem/")
        get_list_response2 = self.client2.get("/budgetitem/")        

        self.assertEqual(len(get_list_response.data), 1)
        list_budget_item = get_list_response.data[0]

        self.assertEqual(len(get_list_response2.data), 1)
        list_budget_item2 = get_list_response2.data[0]

        for attr in list_budget_item:
            self.assertTrue(list_budget_item[attr] == get_response.data[attr])

        for attr in list_budget_item2:
            self.assertTrue(list_budget_item2[attr] == get_response2.data[attr])

        # Step 4: Delete budget items and check that budget items are deleted appropriately
        self.client.delete("/budgetitem/" + str(list_budget_item['id']) + "/")
        self.client2.delete("/budgetitem/" + str(list_budget_item2['id']) + "/")

        post_delete_get_response = self.client.get(budget_item_retrieve_url)
        self.assertEqual(post_delete_get_response.status_code, 404)

        post_delete_get_response2 = self.client2.get(budget_item2_retrieve_url)
        self.assertEqual(post_delete_get_response2.status_code, 404)


    def test_simple_budgetitem_update(self):
        budget_item_CRUD_url = reverse('budget_item_CRUD')
        create_data = { 'name': 'abc', 'date': '2007-01-03', 'description': 'cba', 'value': 213.1 }
        create_response = self.client.post(budget_item_CRUD_url, create_data)
        create_response_data = create_response.data

        budget_item_retrieve_url = "/budgetitem/" + str(create_response.data['id']) + "/"
        get_response = self.client.get(budget_item_retrieve_url)

        for attr in create_response.data:
            self.assertTrue(create_response.data[attr] == get_response.data[attr])
        
        update_data = { 'id': get_response.data['id'], 'user': self.created_user.id, 'name': 'abcd', 'date': '2007-05-03', 'description': 'cbssa', 'value': 390.1 }
        self.client.put(budget_item_retrieve_url, update_data)
        get_response = self.client.get(budget_item_retrieve_url)

        for attr in create_response.data:
            self.assertTrue(update_data[attr] == get_response.data[attr])

    def tearDown(self):
        pass

class UserBudgetSettingTests(APITestCase):
    def setUp(self):
        self.view = BudgetItemView.as_view()
        self.factory = APIRequestFactory()

        # Setup user first
        self.user = User(username="test")
        password = "testing"
        self.user.set_password(password)
        self.user.save()
        self.created_user = User.objects.get(username="test")

        token = Token.objects.get(user_id=self.created_user.id)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

    def test_simple_userbudgetsetting_creation_deletion(self):
        # Step 1: Create user budget setting
        user_budget_setting_url = reverse('user_budget_setting_view')

        # Do not include user in create data, check to see if budget setting is associated with appropriate user automatically
        create_data = { 'cycleLength': 3, 'cycleBudget': 5.5, 'cycleStartDay': 'Monday', 'cycleStartDate': '2017-09-08' }

        create_response = self.client.post(user_budget_setting_url, create_data)
        self.assertEqual(create_response.status_code, 201)

        # Step 2: Compare stored user budget setting to that which was created
        get_response = self.client.get(user_budget_setting_url+str(create_response.data['id'])+'/')
        self.assertEqual(get_response.status_code, 200)
        create_data['user'] = self.created_user.id

        for attr in create_data:
            self.assertTrue(get_response.data[attr] == create_data[attr])

        list_get_response = self.client.get(user_budget_setting_url)
        self.assertEqual(list_get_response.status_code, 200)
        self.assertEqual(len(list_get_response.data),1)

        list_userbudgetsetting_response = list_get_response.data[0]

        for attr in list_userbudgetsetting_response:
            if (attr in create_data):
                self.assertEqual(list_userbudgetsetting_response[attr], create_data[attr])

        # Step 3: Test deleting user budget setting
        self.client.delete(user_budget_setting_url+str(list_userbudgetsetting_response['id'])+'/')

        list_get_response = self.client.get(user_budget_setting_url)
        self.assertEqual(len(list_get_response.data),0)        


    def test_simple_update_userbudgetsetting(self):
        user_budget_setting_url = reverse('user_budget_setting_view')

        create_data = { 'cycleLength': 3, 'cycleBudget': 5.5, 'cycleStartDay': 'Monday', 'cycleStartDate': '2017-09-08' }
        create_response = self.client.post(user_budget_setting_url, create_data)
        create_data['user'] = self.created_user.id

        self.assertEqual(create_response.status_code, 201)

        update_data = { 'id': create_response.data['id'], 'cycleLength': 5, 'cycleBudget': 1.5, 'cycleStartDay': 'Wednesday', 'cycleStartDate': '2019-09-01' }
        
        update_response = self.client.put(user_budget_setting_url+str(update_data['id'])+'/', update_data)

        for attr in update_response.data:
            if (attr in update_data):
                self.assertEqual(update_response.data[attr], update_data[attr])


    def tearDown(self):
        pass