from django.conf.urls import url
from budget_api.views import BudgetItemView, UserBudgetSettingView, BudgetSummaryView, RecurringBudgetItemView
from django.views.generic import TemplateView
from django.conf import settings
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^budgetitem/(?P<id>[0-9]+)/$', BudgetItemView.as_view(), name="budget_item_retrieve_delete"),
    url(r'^budgetitem/$', BudgetItemView.as_view(), name="budget_item_CRUD"),
    url(r'^recurringbudgetitem/$', RecurringBudgetItemView.as_view(), name="recurring_budget_item_CRUD"),
    url(r'^recurringbudgetitem/(?P<id>[0-9]+)/$', RecurringBudgetItemView.as_view(), name="recurring_budget_item_retrieve_delete"),
    url(r'^userbudgetsetting/$', UserBudgetSettingView.as_view(), name="user_budget_setting_view"),
    url(r'^userbudgetsetting/(?P<id>[0-9+])/$', UserBudgetSettingView.as_view(), name="user_budget_setting_retrieve_delete"),
    url(r'^userbudgetsetting/(?P<date>\d{4}-\d{2}-\d{2})/$', UserBudgetSettingView.as_view(), name="user_budget_setting_get_from_date"),
    url(r'^budgetitem/summary/$', BudgetSummaryView.as_view(), name="user_budget_item_summary_get"),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^(?!/?static/)(?!/?media/).+$', TemplateView.as_view(template_name='index.html'))
]
