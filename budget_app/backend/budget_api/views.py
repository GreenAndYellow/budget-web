from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.template import loader
from django.contrib.auth import authenticate, login
from django.db.models import Q
from budget_api.models import BudgetItem, UserBudgetSetting, UserBudgetSummary, RecurringBudgetItem
from budget_api.serializers import BudgetItemSerializer, UserBudgetSettingSerializer, UserBudgetSummarySerializer, RecurringBudgetItemSerializer
from rest_framework import generics, serializers, status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime


class UserBudgetSettingView(mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    serializer_class = UserBudgetSettingSerializer
    lookup_field="id"

    def get_queryset(self):
        user = self.request.user
        return UserBudgetSetting.objects.filter(user_id=user.id)

    def perform_create(self, serializer):
        serializer.save(user = self.request.user)

    def put(self, request, *args, **kwargs):
        try:
            return self.update(request, *args, **kwargs)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        user_budget_setting_id = kwargs.get('id', None)
        date = kwargs.get('date', None)

        if (user_budget_setting_id):
            try:
                user_budget_setting = queryset.get(pk=user_budget_setting_id)
                return Response(UserBudgetSettingSerializer(user_budget_setting).data)
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        elif (date):
            try:
                date_object = datetime.strptime(date, '%Y-%m-%d')
                user_budget_setting = queryset.get(Q(startDate__lte==date_object), Q(endDate__gte==date_object | endDate__isnull == True))
                return Response(UserBudgetSettingSerializer(user_budget_setting).data)
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
            except ValueError:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            user_budget_settings = queryset.all()
            return Response(UserBudgetSettingSerializer(user_budget_settings, many=True).data)
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        user_budget_setting_id = kwargs.get('id', None)
        queryset = self.get_queryset()

        try:
            queryset.get(pk=user_budget_setting_id).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        
class BudgetSummaryView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    lookup_field = "id"

    def get_queryset(self):
        user = self.request.user
        return BudgetItem.objects.filter(user_id=user.id)


    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        start_date = request.GET.get('start_date', None)
        end_date = request.GET.get('end_date', None)

        user_budget_summary = UserBudgetSummary()

        if (not start_date or not end_date):
            return HttpResponseBadRequest(content='A start and end date must be provided for a budget item summary', content_type='text/plain')

        budget_items = queryset.filter(date__gte=start_date, date__lte=end_date)

        for budget_item in budget_items.all():
            if (budget_item.value >= 0):
                user_budget_summary.credit += budget_item.value
            else:
                user_budget_summary.debit += budget_item.value
        
        return Response(UserBudgetSummarySerializer(user_budget_summary).data)

class BudgetItemView(mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    lookup_field = "id"

    serializer_class = BudgetItemSerializer
    queryset = BudgetItem.objects.all()

    def get_queryset(self):
        user = self.request.user
        return BudgetItem.objects.filter(user_id=user.id)

    # Need to extract logged in user from request
    def perform_create(self, serializer):
        serializer.save(user = self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        try:
            return self.update(request, *args, **kwargs)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, *args, **kwargs):
        # Need to confirm that user has access first
        queryset = self.get_queryset()

        budget_item_id = kwargs.get('id', None)

        if (budget_item_id):
            try:
                budget_item = queryset.get(pk=budget_item_id)
                return Response(BudgetItemSerializer(budget_item).data)
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            budget_items = queryset.all()
            return Response(BudgetItemSerializer(budget_items, many=True).data)
    
    def delete(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        budget_item_id = kwargs.get('id', None)

        try:
            queryset.get(pk=budget_item_id).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class RecurringBudgetItemView(mixins.UpdateModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    lookup_field = "id"

    serializer_class = RecurringBudgetItemSerializer
    queryset = RecurringBudgetItem.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        item_id = kwargs.get('id', None)

        if (item_id):
            try:
                recurring_item = queryset.get(pk=item_id)
                return Response(RecurringBudgetItemSerializer(recurring_item).data)
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            recurring_items = queryset.all()
            return Response(RecurringBudgetItemSerializer(recurring_items, many=True).data)

    def perform_create(self, serializer):
        serializer.save(user = self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        try:
            return self.update(request, *args, **kwargs)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        recurring_item_id = kwargs.get('id', None)

        try:
            queryset.get(pk=recurring_item_id).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)






