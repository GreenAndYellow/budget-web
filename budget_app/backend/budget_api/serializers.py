from rest_framework import serializers
from budget_api.models import BudgetItem, UserBudgetSetting, UserBudgetSummary, RecurringBudgetItem

class UserBudgetSettingSerializer(serializers.ModelSerializer):
    def validate_cycleStartDay(self, value):
        daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

        if not value in daysOfWeek:
            raise serializers.ValidationError("The cycleStartDay must be a valid day of the week")
        else:
            return value

    def validate_cycleLength(self, value):
        if (value < 1):
            raise serializers.ValidationError("The cycle length must be at least 1")
        else:
            return value

    def validate_cycleBudget(self, value):
        if (value < 0):
            raise serializers.ValidationError("The cycle budget must be at least 0")
        else:
            return value

    class Meta:
        model = UserBudgetSetting
        fields = ('id','cycleLength','cycleBudget','cycleStartDay','cycleStartDate','user')

class BudgetItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BudgetItem
        fields = ('id','date','name','description','value','user')

class RecurringBudgetItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecurringBudgetItem
        fields = ('id','startDate','endDate','name','cycleLength','description','value','user')

class UserBudgetSummarySerializer(serializers.Serializer):
    credit = serializers.FloatField()
    debit = serializers.FloatField()




    

