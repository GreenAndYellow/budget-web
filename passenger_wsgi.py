import sys, os
cwd = os.getcwd()
sys.path.append(cwd)
sys.path.append(cwd + '/budget_app/backend')

INTERP = "/home/tianlingshell/codeling.me/budget_app_env/bin/python3"

if sys.executable != INTERP:
	os.execl(INTERP, INTERP, *sys.argv)

sys.path.insert(0,cwd+'/budget_app_env/bin')
sys.path.insert(0,cwd+'/budget_app_env/lib/python3.6/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = "budget_app.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
